require 'test_helper'

class CommentTest < ActiveSupport::TestCase
  test "should not save comment without post_id or body" do
    comment = Comment.new
    assert_not comment.save
   
    comment.post_id = 1
    assert_not comment.save
   
    comment.body = 'Test body'
    assert comment.save
  end
end
