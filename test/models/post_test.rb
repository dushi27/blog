require 'test_helper'

class PostTest < ActiveSupport::TestCase
 test "should not save post without title or body" do
    post = Post.new
    assert_not post.save
   
    post.title = 'Test'
    assert_not post.save
   
    post.body = 'Test body'
    assert post.save
  end
end
